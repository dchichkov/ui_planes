cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(plane_extraction)

set(CMAKE_CXX_STANDARD 11)
set(GLFW3_DIR /usr/lib/x86_64-linux-gnu/cmake/glfw3/)

# Dependencies
find_package(PCL 1.7 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(glfw3 REQUIRED)

# Include PCL files
include_directories(${PCL_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR})
link_directories(${PCL_LIBRARY_DIRS} ${GLFW3_LIBRARY_DIR} ${CMAKE_SOURCE_DIR})
add_definitions(${PCL_DEFINITIONS})

set(SOURCE_FILES ${CMAKE_SOURCE_DIR}/main.cpp
	${CMAKE_SOURCE_DIR}/geom/polygons.cpp
	${CMAKE_SOURCE_DIR}/geom/matutils.cpp
	${CMAKE_SOURCE_DIR}/geom/eig3.cpp
    	${CMAKE_SOURCE_DIR}/geom/convexhull.cpp
	${CMAKE_SOURCE_DIR}/geom/cluster_extraction.cpp
	${CMAKE_SOURCE_DIR}/geom/linmath.h
 )


include_directories(${GLFW3_INCLUDE_DIR})

add_executable (plane_extraction ${SOURCE_FILES})
# target_link_libraries (plane_extraction  /usr/local/lib/librealsense.so ${PCL_LIBRARIES} ${GLFW3_LIBRARY} ${OPENGL_LIBRARIES} ${GLFW_STATIC_LIBRARIES})
target_link_libraries (plane_extraction /usr/local/lib/librealsense.so ${PCL_LIBRARIES} ${OPENGL_LIBRARIES} ${GLFW_STATIC_LIBRARIES} ${GLFW3_LIBRARY} libglad.a )
