// R200

#include <librealsense/rs.hpp>


// OpenGL
// from http://www.glfw.org/docs/latest/quick.html
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "geom/linmath.h"
#include <stdlib.h>
#include <stdio.h>
#include "geom/polygons.h"
#include "geom/cluster_extraction.h"
#include <iostream>
#include <fstream>
#include <string>

#define MAX_DRAWN 3

void pclCloudToStruct(XYZCloudPtr &cloud, std::vector<point*> &result) {
  for (auto &p0: *cloud) {
    auto p = new point;
    p->x = p0.x;
    p->y = p0.y;
    p->z = p0.z;
    result.push_back(p);
  }
}

void convertR200ToPcd(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud,
					  const uint16_t* depth_image,
					  uint32_t width, uint32_t height,
                      rs::intrinsics& depth_intrin,
                      const float scale) {
    // r200 - to - pcl
    cloud.reset(new pcl::PointCloud<pcl::PointXYZ>(width, height));
    cloud->is_dense = false;
    cloud->points.resize(width * height);
    float max_distance = 3.5; 	// filter out data

    int num_point = 0;

    for (int dy = 0; dy < height; ++dy) {
        for (int dx = 0; dx < width; ++dx) {
            uint i = dy * width + dx;
            uint16_t depth_value = depth_image[i];
            float depth_in_meters = depth_value * scale;

            rs::float2 depth_pixel = { (float)dx, (float)dy };
            rs::float3 depth_point = depth_intrin.deproject(depth_pixel, depth_in_meters);

            static const float nan = std::numeric_limits<float>::quiet_NaN();
            if (depth_value == 0 || depth_point.z > max_distance) {
                cloud->points[num_point].x = nan;
                cloud->points[num_point].y = nan;
                cloud->points[num_point].z = nan;
                num_point++;
                continue;
            }
            else
            {
                cloud->points[num_point].x = depth_point.x;
                cloud->points[num_point].y = depth_point.y;
                cloud->points[num_point].z = depth_point.z;
                num_point++;
            }
        }
    }
    cloud->points.resize(num_point);
}


static const struct
{
    float x, y;
    float r, g, b;
} vertices[4] =
{
    { -0.6f, -0.4f, 1.f, 0.f, 0.f },
    {  0.6f, -0.4f, 0.f, 1.f, 0.f },
    { 0.6, -0.2, 0, 1, 0},
    {   0.f,  0.6f, 0.f, 0.f, 1.f },
};
static const char* vertex_shader_text =
"uniform mat4 MVP;\n"
"attribute vec3 vCol;\n"
"attribute vec2 vPos;\n"
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
"    color = vCol;\n"
"}\n";
static const char* fragment_shader_text =
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_FragColor = vec4(color, 1.0);\n"
"}\n";
static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, 1);
}


int main(int argc, char *argv[]) try
{
    // Bring up R200
    
    rs::log_to_console(rs::log_severity::warn);
    rs::context ctx;
    if(ctx.get_device_count() < 1) throw std::runtime_error("No R200 device found. Is it plugged in?");
    rs::device *dev = ctx.get_device(0);
    dev->enable_stream(rs::stream::depth, rs::preset::best_quality);
    rs::intrinsics depth_intrin = dev->get_stream_intrinsics(rs::stream::depth);
    dev->start();

    // Bring up OpenGL
    GLFWwindow* window;
    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
    GLint mvp_location, vpos_location, vcol_location;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
       throw std::runtime_error("glfwInit failed");
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(1280, 800, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        throw std::runtime_error("glfwCreateWindow failed");
    }
    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);
    // NOTE: OpenGL error checks have been omitted for brevity
    glGenBuffers(1, &vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
    glCompileShader(vertex_shader);
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
    glCompileShader(fragment_shader);
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    mvp_location = glGetUniformLocation(program, "MVP");
    vpos_location = glGetAttribLocation(program, "vPos");
    vcol_location = glGetAttribLocation(program, "vCol");
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(float) * 5, (void*) 0);
    glEnableVertexAttribArray(vcol_location);
    glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE,
                          sizeof(float) * 5, (void*) (sizeof(float) * 2));

    float s = 0.5;
    while (!glfwWindowShouldClose(window))
    {
        // Get Depth Data from R200
        
        if(!dev->is_streaming()) {
            throw std::runtime_error("R200 device is not streaming data?");
        }

        dev->wait_for_frames();
        uint32_t width = dev->get_stream_width(rs::stream::depth);
        uint32_t height = dev->get_stream_height(rs::stream::depth);
        uint16_t *depth_image = (uint16_t*) dev->get_frame_data(rs::stream::depth);
        float scale = 0.001;	// check
        

        // Convert to PCL
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
        convertR200ToPcd(cloud, depth_image, width, height, depth_intrin, scale);

        // Segmentation
        std::vector<XYZCloudPtr> segments;
        segment(cloud, segments);
	
        float ratio;
        int w,h;
        mat4x4 m, p, mvp;
        glfwGetFramebufferSize(window, &w, &h);
        ratio = w / (float) h;
        glViewport(0, 0, w, h);
        glClear(GL_COLOR_BUFFER_BIT);
        int n_drawn = segments.size() > MAX_DRAWN? MAX_DRAWN : segments.size();
        for (int idx = 0; idx < n_drawn; idx++) {
            XYZCloud c,hull;
            pclCloudToStruct(segments[idx],c);
            findPolygon(c,hull);
	    glColor3f(1.0/((float) idx+1),0,0);
            glBegin(GL_POLYGON);
            for (auto &p: hull) {
                glVertex3f(s*p->x, -s*p->y, s*p->z);
            }
            glEnd();
        }

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

catch(const rs::error & e)
{
std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n" << e.what() << std::endl;
return EXIT_FAILURE;
}
catch(const std::exception & e)
{
std::cerr << e.what() << std::endl;
return EXIT_FAILURE;
}
