#include "cluster_extraction.h"
#include <iostream>

void segment (XYZCloudPtr &cloud, std::vector<XYZCloudPtr> &segments)
{
  XYZCloudPtr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

  // Downsample the dataset with a voxel grid.
  pcl::VoxelGrid<pcl::PointXYZ> vg;
  XYZCloudPtr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  vg.setInputCloud (cloud);
  vg.setLeafSize (0.03f, 0.03f, 0.03f);
  vg.filter (*cloud_filtered);

  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  XYZCloudPtr cloud_plane;

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (100);
  seg.setDistanceThreshold (0.02);

  int i=0, nr_points = (int) cloud_filtered->points.size ();

  while (cloud_filtered->points.size() > 0.3 * nr_points)
  {
    XYZCloudPtr cloud_plane(new pcl::PointCloud<pcl::PointXYZ> ());
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_filtered);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers);
    extract.setNegative (false);

    // Get the points associated with the planar surface
    extract.filter (*cloud_plane);
    segments.push_back(cloud_plane);

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_f);
    *cloud_filtered = *cloud_f;
  }

}