#include "polygons.h"

/*
 * Algorithm due to Joydeep Biswas, Manuela Veloso
 * Planar Polygon Extraction and Merging from Depth Images (2012)
 * http://repository.cmu.edu/compsci/2806/
 */

// TODO: replace files in this folder with library functions where already available

point* centroid(XYZCloud &cloud) {
    point* c = new point;
    double scale = 1.0 / ((double) cloud.size());
    c->x = 0;
    c->y = 0;
    c->z = 0;
    for (auto &p: cloud) {
        c->x += p->x;
        c->y += p->y;
        c->z += p->z;
    }
    c->x *= scale;
    c->y *= scale;
    c->z *= scale;
    return c;
}

// Computes a covariance-like matrix.
// The eigenvalues of this matrix will fit the plane with a coordinate system
// and normal:
// Since smallest eigenvalue corresponds to the vector in direction of least
// variance, on a plane we take this to be the normal.
void scatter(XYZCloud &cloud, mat3 &S, point* c) {
    zero(S);
    double p_rel[3];
    for (auto &p: cloud) {
        p_rel[0] = p->x - c->x;
        p_rel[1] = p->y - c->y;
        p_rel[2] = p->z - c->z;
        for (int i=0; i < 3; i ++) {
            for (int j=0; j < 3; j ++) {
                S[i][j] += (p_rel[i]) * (p_rel[j]);
            }
        }
    }

    // Scale the matrix by 1/|cloud|
    for (int i=0; i < 3; i ++) {
        for (int j=0; j < 3; j ++) {
            S[i][j] *= 1.0/((double) cloud.size());
        }
    }
}

void rebasePoint(point &p, point& c, mat3 &T, point &n_orig, point *p_result) {
    point p_proj = p;
    point p_rel = p;
    point n = n_orig;

    subtract(p_rel,c);
    double d = dotProd(p_rel,n);
    scale(n,d);
    subtract(p_proj,n);

    transform(T,p_proj,p_result);
    // Remove normal component (smallest eigenvalued eigenvector was
    // in the x position)
    double temp = p_result->x;
    p_result->x = p_result->z;
    p_result->z = temp;
}

XYZCloud rebasePoints(XYZCloud &cloud, point &c, mat3 &T, point &n) {
    XYZCloud rebased_cloud;
    for (auto &p: cloud) {
        point* p_T = new point;
        rebasePoint(*p,c,T,n,p_T);
        rebased_cloud.push_back(p_T);
    }
    return rebased_cloud;
}

void findPolygon(XYZCloud &cloud, XYZCloud &hull) {
    mat3 S, V, I;
    point* c = centroid(cloud);
    scatter(cloud,S,c);

    // Find V = matrix of eigenvalues used as basis/normal for plane.
    // The inverse, I, is stored for change of basis.
    double d[3];
    eigen_decomposition(S, V, d);

    // Set normal according to the eigenvector with smallest eigenvalue,
    // which is always placed in the first column of V in this implementation.
    point n_orig;
    n_orig.x = V[0][0];
    n_orig.y = V[1][0];
    n_orig.z = V[2][0];

    // Set vector to centroid as third element of basis
    V[0][0] = c->x;
    V[0][1] = c->y;
    V[0][2] = c->z;

    inverse(V,I);
    XYZCloud rebased_cloud = rebasePoints(cloud,*c,I,n_orig);
    convex_hull(rebased_cloud, hull);
    cloud = rebased_cloud;

    // Convert back to original basis
    double temp;
    for (auto &p: hull) {
        // Switch because the smallest eigenvalued eigenvector in first col of
        // V.
        temp = p->z;
        p->z = p->x;
        p->x = temp;
        transform(V,*p);
    }
}
