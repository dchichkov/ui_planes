cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(plane_extraction)

set(CMAKE_CXX_STANDARD 11)

# Dependencies
find_package(PCL 1.7 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLFW3 REQUIRED)

# Include PCL files
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS} ${GLFW3_LIBRARY_DIR})
add_definitions(${PCL_DEFINITIONS})

add_executable (plane_extraction main.cpp)
target_link_libraries (plane_extraction  /usr/local/lib/librealsense.so ${PCL_LIBRARIES} ${GLFW3_LIBRARY} ${OPENGL_LIBRARIES} ${GLFW_STATIC_LIBRARIES})
target_include_directories(${GLFW3_INCLUDE_DIR})
