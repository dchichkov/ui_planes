#include "matutils.h"
#include <algorithm>

/*
 * Implementation of monotone chain for a vector of floats
 */

XYZCloud convex_hull(XYZCloud &points, XYZCloud &hull);