#include <vector>

typedef double mat3[3][3];
struct point {
    double x,y,z;
};
typedef std::vector<point*> XYZCloud;
static inline int max(int x, int y);
static inline int min(int x, int y);

void zero(mat3 &M);
void inverse(mat3 &M, mat3 &I);
// Sets X = X - Y
void subtract(point &X, point &Y);
void scale(point &X, double s);
double dotProd(point &X, point &Y);
point* projectPoint(point &c, point &p, mat3 basis);
// Sets *Y = TX
void transform(mat3 &T, point &X, point *Y);
void transform(mat3 &T, point &X);